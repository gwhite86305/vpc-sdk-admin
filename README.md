# VpcSdkAdmin

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Testing:

   The server must be running at http://localhost:5000

    To test REST API endpoint -  http://localhost:5000/api/partners   - A collection of partners

   Open a browser at http://localhost:4200
   
   VpcSdkAdmin displays a list of Partners.

## REST API's

      http://localhost:5000/api/partners - A collection of Partners

      http://localhost:5000/api/partner - GET - Gets a partner based on a partner key field. Uses X-AGI-PARTNER-KEY key field in Header.

      http://localhost:5000/api/users - GET - Gets all Users in User table.

      http://localhost:5000/api/users/register - POST - Add a User to User table.

      http://localhost:5000/api/login - POST - Validates a User in the user table given a username and password.   

      http://localhost:5000/api/logout - POST - Log's out a user and refreshes the token list.  

   See test results in Partner-test-results.png  