export const OCCASIONS: any[] =
[
  {
    id: '1',
    title: 'BirthDay',
    partner_id: '3',
    start_date: '2017-01-01 00:00:00',
    end_date: '2018-02-26 00:00:00',
    sort_order: '2'
  },
  {
    id: '2',
    title: 'Just Because',
    partner_id: '3',
    start_date: '2017-01-01 00:00:00',
    end_date: '2018-02-26 00:00:00',
    sort_order: '1'
  },
  {
    id: '3',
    title: 'Thank You',
    partner_id: '3',
    start_date: '2017-01-01 00:00:00',
    end_date: '2018-02-26 00:00:00',
    sort_order: '3'
  },
  {
    id: '4',
    title: 'Congradulations',
    partner_id: '3',
    start_date: '2017-01-01 00:00:00',
    end_date: '2018-02-26 00:00:00',
    sort_order: '0'
    },
]