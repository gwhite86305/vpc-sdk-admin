import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule, MatDatepickerModule, MatNativeDateModule, MatInputModule, MatPaginatorModule, MatSortModule,
  MatTableModule, MatToolbarModule,
} from '@angular/material';
import { AppComponent } from './app.component';
import { PartnerTableComponent } from './partner-table/partner-table.component'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { PartnerService } from './services/partner.service';
import { AlertService } from './services/alert.service';
import { UserService } from './services/user.service';
import { AuthenticationService } from './services/authentication.service';
import { PartnerKeyService } from './services/partnerkey.service';
import { ProductService } from './services/product.service';
import { OccasionService } from './services/occasion-service';
import { DataService } from './services/dataservice';

import { AddPartnerDialogComponent} from './dialogs/add/Partners/addPartner.dialog.component';
import { EditPartnerDialogComponent} from './dialogs/edit/Partners/editPartner.dialog.component';
import { AddOccasionDialogComponent } from './dialogs/add/Occasions/addOccasion.dialog.component';
import { EditOccasionDialogComponent } from './dialogs/edit/Occasions/editOccasion.dialog.component';
//import { fakeBackendProvider } from './_helpers';

import { routing } from './app.routing';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertComponent } from './_directives';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { NavbarComponent } from './navbar/navbar.component';
import { OccasionComponent } from './occasion/occasion.component';
import { ProductComponent } from './product/product.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AddPartnerDialogComponent,
    EditPartnerDialogComponent,
    AddOccasionDialogComponent,
    EditOccasionDialogComponent,
    PartnerTableComponent,
    NavbarComponent,
    OccasionComponent,
    ProductComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    routing,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatSortModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatPaginatorModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    AddPartnerDialogComponent,
    AddOccasionDialogComponent,
    EditPartnerDialogComponent,
    EditOccasionDialogComponent
  ],  
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    PartnerService, AlertService, AuthenticationService, PartnerKeyService, UserService, OccasionService, ProductService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
