import {MAT_DIALOG_DATA, MatDialogRef, MatDialogModule } from '@angular/material';
import {Component, Inject} from '@angular/core';
import { OccasionService } from '../../../services/occasion-service';
import { FormControl, Validators} from '@angular/forms';
import { IOccasion } from '../../../core/interfaces/occasions.inferface';

@Component({
  selector: 'app-add-occasion.dialog',
  templateUrl: 'addOccasion.dialog.html',
  styleUrls: ['addOccasion.dialog.css']
})

export class AddOccasionDialogComponent {
  constructor(public dialogRef: MatDialogRef<AddOccasionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: IOccasion,
              public dataService: OccasionService) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('title') ? 'Not a valid title' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addOccasion(this.data);
  }
}
