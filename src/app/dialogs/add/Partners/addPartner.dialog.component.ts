import {MAT_DIALOG_DATA, MatDialogRef, MatDialogModule } from '@angular/material';
import {Component, Inject} from '@angular/core';
import { PartnerService } from '../../../services/partner.service';
import { FormControl, Validators} from '@angular/forms';
import { Partner } from '../../../models/partner.model';

@Component({
  selector: 'app-add-partner.dialog',
  templateUrl: 'addPartner.dialog.html',
  styleUrls: ['addPartner.dialog.css']
})

export class AddPartnerDialogComponent {
  constructor(public dialogRef: MatDialogRef<AddPartnerDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Partner,
              public dataService: PartnerService) { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('title') ? 'Not a valid title' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addPartner(this.data);
  }
}
