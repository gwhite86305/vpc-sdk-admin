import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, AfterViewInit, ViewChild, Inject} from '@angular/core';
import {PartnerService} from '../../../services/partner.service';
import {PartnerKeyService} from '../../../services/partnerkey.service';
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-partner-dialog',
  templateUrl: 'editPartner.dialog.html',
  styleUrls: ['editPartner.dialog.css']
})
export class EditPartnerDialogComponent implements AfterViewInit {

  constructor(public dialogRef: MatDialogRef<EditPartnerDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,  public dataService: PartnerService, public myService: PartnerKeyService, private router: Router) {
                this.myService.myMethod(this.data.partner_key);
               }
 
  public partnerdata: string;

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  ngAfterViewInit() {
     console.log('edit Partner Dialog ngAfterViewInit partner key is' + this.data.partner_key);
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
    // empty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  Occasions(partner_key): void {
      console.log("EditDialogComponent: Occasions partner key " + partner_key);
      this.router.navigate(['/occasions/', partner_key]);
  }

  stopEdit(): void {
    this.dataService.updatePartner(this.data);
  }

}
