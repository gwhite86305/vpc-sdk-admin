import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, AfterViewInit, ViewChild, Inject} from '@angular/core';
import {OccasionService } from '../../../services/occasion-service';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { Router } from '@angular/router';
import { FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-edit-occasion-dialog',
  templateUrl: './editOccasion.dialog.html',
  styleUrls: ['./editOccasion.dialog.css']
})
export class EditOccasionDialogComponent implements AfterViewInit {

  constructor(public dialogRef: MatDialogRef<EditOccasionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: OccasionService, private router: Router )
               { }
 
  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  ngAfterViewInit() {
     console.log('editOccasion Dialog ngAfterViewInit called');  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('fielderror') ? 'Not a valid field' :
        '';
  }

  submit() {
    // empty stuff
  }

  Products(data_id): void {
    console.log("EditOccasionDialogComponent: data_id " + data_id);
    this.router.navigate(['/products/', data_id]);
  }

  onNoOccasionClick(): void {
    this.dialogRef.close();
  }
  
  stopOccasionEdit(): void {
    this.dataService.updateOccasion(this.data);
  }

}
