import { Component, Inject, ElementRef, Input, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { IRootObject } from '../core/interfaces/occasions.inferface';
import { IOccasion } from '../core/interfaces/occasions.inferface';
import { ProductsItem } from '../core/interfaces/occasions.inferface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
import { DataSource} from '@angular/cdk/collections';
import { ProductService } from '../services/product.service';
import { DataService } from '../services/dataservice';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
  
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';

  @Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
  })
  
  
  export class ProductComponent implements OnInit, AfterViewInit {
      displayedColumns = ['content_id', 'cover_verse', 'thumb', 'actions'];
      productDatabase: ProductService | null;
      dataSource: ProductDataSource | null;
  
   // Material fields 
      index: number;
      id: number;
      cover_verse: string;
      title: string;
      content_id: number;
      assets: number;
      thumb: string;
      selectedproducts: IOccasion;
      productData: ProductsItem[]; 
//      data_products: ProductsItem;
      data_id: number;

      @Input() data;
      sub;
      loading = false;
      submitted = false;
      returnUrl: string;
      error = '';
   
      constructor(@Inject(DOCUMENT) private document: any,
          public service: ProductService,
          public dataservice: DataService,
          public httpClient: HttpClient,
          public dialog: MatDialog,
          private route: ActivatedRoute,
          private router: Router)
          { }
  
      @ViewChild(MatPaginator) paginator: MatPaginator;
      @ViewChild(MatSort) sort: MatSort;
      @ViewChild('filter') filter;  
  
    
      ngOnInit() {
  
         this.sub = this.route.params.subscribe(params => {
             this.data_id = params['data_id'];
         });
  
      console.log('Product component ngInit Called: data_id passed is ' + this.data_id);
   // get return url from route parameters or default to '/'
  
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  
        this.service.getProducts().subscribe((data) => {
    //       console.log('data ' + data[0].id);
           this.productData = data;
           this.loadData(this.productData);
         });
  
      }

      refresh() {
          this.loadData(this.productData);
      }
  
      onSubmit() {
          this.submitted = true;
          this.loading = true;
      }
  
      ngAfterViewInit() {
      }
 
      applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
      }

      goToUrl(content_id): void {
        let url = "http://mss.ag.com/c3/content/edit/" + content_id;
        console.log('goToUrl: url is: ' + url)
      //  $(location).attr('href', url);
     //
        window.open(url);
    //    window.location.replace(url);
       // this.document.location.href = url;
    }

    // If you don't need a filter or a pagination this can be simplified, you just use code from else block
      private refreshTable() {
    // if there's a paginator active we're using it for refresh
        if (this.dataSource._paginator.hasNextPage()) {
          this.dataSource._paginator.nextPage();
          this.dataSource._paginator.previousPage();
    // in case we're on last page this if will tick
        } else if (this.dataSource._paginator.hasPreviousPage()) {
          this.dataSource._paginator.previousPage();
          this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
        } else {
          this.dataSource.filter = '';
          this.dataSource.filter = this.filter.nativeElement.value;
        }
      }

    startProductEdit(i: number, id: number, title: string, cover_verse: string, inside_verse: string, content_id: number) {
      this.id = id;
      // index row is used just for debugging proposes and can be removed
      this.index = i;
      console.log(this.index);
     // const dialogRef = this.dialog.open(EditOccasionDialogComponent, {
     //   data: {id: id, title: title, start_date: start_date, end_date: end_date, sort_order: sort_order}
     // });

    }  

    public loadData(data2: ProductsItem[]) {
      this.productDatabase = new ProductService(this.dataservice);
      this.dataSource = new ProductDataSource(this.productDatabase, data2, this.paginator, this.sort);

          Observable.fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(() => {
              if (!this.dataSource) {
                return;
              }
              this.dataSource.filter = this.filter.nativeElement.value;
            });
      }
  }

export class ProductDataSource extends DataSource<ProductsItem> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: ProductsItem[] = [];
  renderedData: ProductsItem[] = [];

  constructor(public _productDatabase: ProductService,
              public _products: ProductsItem[], 
              public _paginator: MatPaginator,
              public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<ProductsItem[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._productDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._productDatabase.dataChange.next(this._products);

    return Observable.merge(...displayDataChanges).map(() => {
      // Filter data
    //  this.filteredData = this._products;
      this.filteredData = this._products.slice().filter((product: ProductsItem) => {
        const searchStr = (product.content_id + product.title).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
 //     this.renderedData = this._products.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    });
  }
  disconnect() {
  }
  /** Returns a sorted copy of the partner database data. */
  sortData(data: ProductsItem[]): ProductsItem[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
          
      switch (this._sort.active) {
        case 'content_id': [propertyA, propertyB] = [a.content_id, b.content_id]; break;
        case 'title': [propertyA, propertyB] = [a.title, b.title]; break;
      
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
