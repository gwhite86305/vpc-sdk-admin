  import { Component, ElementRef, Input, OnInit, ViewChild, AfterViewInit } from '@angular/core';
  import { Observable } from 'rxjs/Observable';
  import { Router, ActivatedRoute } from '@angular/router';
  import { IItem } from '../core/interfaces/occasions.inferface';
  import { IOccasion } from '../core/interfaces/occasions.inferface';
  import { Occasion } from '../../app/models/occasion.model';
  import { FormBuilder, FormGroup, Validators } from '@angular/forms';
  import { BehaviorSubject} from 'rxjs/BehaviorSubject';
  import { DataSource} from '@angular/cdk/collections';
  import { AddOccasionDialogComponent } from '../dialogs/add/Occasions/addOccasion.dialog.component';
  import { EditOccasionDialogComponent } from '../dialogs/edit/Occasions/editOccasion.dialog.component';
  import { PartnerKeyService } from '../../app/services/partnerkey.service';
  import { OccasionService } from '../services/occasion-service';
  import { DataService } from '../services/dataservice';
  import { HttpClient } from '@angular/common/http';
  import { MatDialog, MatPaginator, MatSort } from '@angular/material';
  
  import 'rxjs/add/observable/merge';
  import 'rxjs/add/observable/fromEvent';
  import 'rxjs/add/operator/debounceTime';
  import 'rxjs/add/operator/distinctUntilChanged';
  import 'rxjs/add/operator/map';
  import 'rxjs/add/observable/of';
  import 'rxjs/add/operator/delay';
  import * as cloneDeep from 'lodash/cloneDeep';
  
  @Component({
      selector: 'app-occasion',
      templateUrl: 'occasion.component.html',
      styleUrls: ['./occasion.component.css']
  })
  
  export class OccasionComponent implements OnInit, AfterViewInit {
      displayedColumns = ['id', 'title', 'start_date', 'end_date', 'sort_order', 'actions'];
      occasionDatabase: OccasionService | null;
      dataSource: OccasionDataSource | null;
  
   // Material fields   
      index: number;
      id: number;
      title: string;
      partner_id: number;
      start_date: Date;
      end_date: Date;
      sort_order: number;

      occasionsData: IOccasion[]; 
      @Input() productsData: IOccasion;
      @Input() data;
      sub;
      @Input() partner_key: string;
      loading = false;
      submitted = false;
      returnUrl: string;
      error = '';
   
      constructor(public service: OccasionService,
          public httpClient: HttpClient,
          public dialog: MatDialog,
          private route: ActivatedRoute,
          public myService: PartnerKeyService,
          public dataService: DataService,
          private router: Router)
          { }
  
      @ViewChild(MatPaginator) paginator: MatPaginator;
      @ViewChild(MatSort) sort: MatSort;
      @ViewChild('filter') filter: ElementRef;  
  
      set(value: IOccasion) {
        this.dataService.selectedproducts = value;
      }  


      ngOnInit() {
  
         this.sub = this.route.params.subscribe(params => {
             this.partner_key = params['partner_key'];
         });
  
        console.log('Occasions component ngInit Called: partner_key passed is ' + this.partner_key);
   // get return url from route parameters or default to '/'
  
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  
        this.service.getrootobject(this.partner_key).subscribe((data) => {
           console.log('data ' + data[0].id);
           this.occasionsData = data;
           console.log("Occasions component this.occasions id is: " + this.occasionsData[0].id);
           this.loadData(this.occasionsData);
         });
  
      }

      refresh() {
          this.loadData(this.occasionsData);
      }
        
  
        onSubmit() {
          this.submitted = true;
          this.loading = true;
      }
  
      ngAfterViewInit() {
  
      }

      addNew(occasion: IOccasion) {
        const dialogRef = this.dialog.open(AddOccasionDialogComponent, {
          data: {occasion: occasion }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if (result === 1) {
            // After dialog is closed we're doing frontend updates
            // For add we're just pushing a new row inside PartnerTableService
            this.occasionDatabase.dataChange.value.push(this.service.getDialogData() );
            this.refreshTable();
          }
        });
      }

      // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    private refreshTable() {
    // if there's a paginator active we're using it for refresh
      if (this.dataSource._paginator.hasNextPage()) {
        this.dataSource._paginator.nextPage();
        this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
      } else if (this.dataSource._paginator.hasPreviousPage()) {
        this.dataSource._paginator.previousPage();
        this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
      } else {
        this.dataSource.filter = '';
        this.dataSource.filter = this.filter.nativeElement.value;
      }
    }
      startOccasionEdit(i: number, id: number, title: string, start_date: string, end_date: string, sort_order: number, products: Object) {
        this.id = id;
        // index row is used just for debugging proposes and can be removed
        this.index = i;
        console.log(this.index);

        for(let i = 0, l = this.occasionsData.length; i < l; i++) {
          var obj: IOccasion = this.occasionsData[i];
          if (obj.id = this.occasionsData[i].id ) {  
             this.productsData = obj;
          }
          this.set(this.productsData);

        }

        const dialogRef = this.dialog.open(EditOccasionDialogComponent, {
          data: {id: id, title: title, start_date: start_date, end_date: end_date, sort_order: sort_order, products: this.productsData}
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if (result === 1) {
            // When using an edit things are little different, firstly we find record inside DataService by id
            const foundIndex = this.occasionDatabase.dataChange.value.findIndex(x => x.id === this.id);
            // Then you update that record using data from dialogData (values you enetered)
            this.occasionDatabase.dataChange.value[foundIndex] = this.service.getDialogData();
            // And lastly refresh table
            this.refreshTable();
          }
        });


      }
  
      public loadData(data2: IOccasion[]) {
          this.occasionDatabase = new OccasionService(this.httpClient);
          this.dataSource = new OccasionDataSource(this.occasionDatabase, data2, this.paginator, this.sort);
          Observable.fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(() => {
              if (!this.dataSource) {
                return;
              }
              this.dataSource.filter = this.filter.nativeElement.value;
            });
        }
  }
  
  export class OccasionDataSource extends DataSource<IOccasion> {
      _filterChange = new BehaviorSubject('');
    
      get filter(): string {
        return this._filterChange.value;
      }
    
      set filter(filter: string) {
        this._filterChange.next(filter);
      }
    
      filteredData: IOccasion[] = [];
      renderedData: IOccasion[] = [];
    
      constructor(public _occasionDatabase: OccasionService,
                  public _occasions: IOccasion[], 
                  public _paginator: MatPaginator,
                  public _sort: MatSort) {
        super();
        // Reset to the first page when the user changes the filter.
        this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
      }
    
      /** Connect function called by the table to retrieve one stream containing the data to render. */
      connect(): Observable<IOccasion[]> {
        // Listen for any changes in the base data, sorting, filtering, or pagination
        const displayDataChanges = [
          this._occasionDatabase.dataChange,
          this._sort.sortChange,
          this._filterChange,
          this._paginator.page
        ];
    
        this._occasionDatabase.getAllOccasions(this._occasions);
    
        return Observable.merge(...displayDataChanges).map(() => {
    // Filter data
          this.filteredData = this._occasionDatabase.data.slice().filter((occasion: IOccasion) => {
            const searchStr = (occasion.id + occasion.title).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
    
          // Sort filtered data
          const sortedData = this.sortData(this.filteredData.slice());
    
          // Grab the page's slice of the filtered sorted data.
          const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
          this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
          return this.renderedData;
        });
      }
      disconnect() {
      }
      /** Returns a sorted copy of the partner database data. */
      sortData(data: IOccasion[]): IOccasion[] {
        if (!this._sort.active || this._sort.direction === '') {
          return data;
        }
    
        return data.sort((a, b) => {
          let propertyA: number | string = '';
          let propertyB: number | string = '';
              
          switch (this._sort.active) {
            case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
            case 'title': [propertyA, propertyB] = [a.title, b.title]; break;
          
          }
    
          const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
          const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
    
          return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
        });
      }
    }









  