import { TestBed, inject } from '@angular/core/testing';

import { OccasionServiceService } from './occasion-service';

describe('OccasionServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OccasionServiceService]
    });
  });

  it('should be created', inject([OccasionServiceService], (service: OccasionServiceService) => {
    expect(service).toBeTruthy();
  }));
});
