import { Injectable } from '@angular/core';
import { DataService } from '../services/dataservice';
import { IOccasion } from '../core/interfaces/occasions.inferface';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IProduct } from '../core/interfaces/product.interface';
import { ProductsItem } from '../core/interfaces/occasions.inferface';
import { first } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep'; 
import * as clone from 'clone';
import 'rxjs/add/operator/map';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ProductService {
  constructor(public dataservice: DataService ) { }

  dataChange: BehaviorSubject<ProductsItem[]> = new BehaviorSubject<ProductsItem[]>([]);
  selectedproduct: IOccasion;
  newProducts: ProductsItem[];
  
  public getProducts(): Observable<ProductsItem[]>{
     this.newProducts = this.dataservice.selectedproducts['products']; 
     
  //   this.newProducts = 
  //   [
  //     {id: 22, cover_verse: 'There are so many reasons to celebrate you', inside_verse:"...but your birthday's definitely my favorite.", title: 'Add-a-Card SDK Core Collection DO NOT TAG', content_id: 3480369},
  //     {id: 22, cover_verse: "I feel the burn!", inside_verse: "Wishing you every happy thing you can think of. (Plus some extra.)", title: "SDK PRODUCT DO NOT TAG", content_id: 3480371},
  //     {id: 22, cover_verse: "Someone as awesome as you, deserves the awesomest birthday", inside_verse: "Enjoy!", title: 'Add-a-Card SDK Core Collection DO NOT TAG', content_id: 3480372},
  //     {id: 22, cover_verse: "Happy Birthday to You", inside_verse: "Wishing you the best, brightest, and happiest kind of birthday!", title: 'SDK PRODUCT DO NOT TAG', content_id: 3481354},
  //     {id: 22, cover_verse: " Hip, hip, hooray - it's your birthday today!", inside_verse: "Wishing you your best birthday yet!", title: 'SDK PRODUCT DO NOT TAG', content_id: 3481365}
  //   ];

     this.dataChange.next(this.newProducts);

     return Observable.of(this.newProducts).delay(500);
  }

  get data(): ProductsItem[] {
    return this.newProducts;
  }

  getAllProducts(products: ProductsItem[]): void {
    this.dataChange.next(products);
}

}
