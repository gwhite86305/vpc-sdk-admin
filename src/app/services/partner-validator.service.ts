import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidatorService } from 'angular4-material-table';

@Injectable()
export class PartnerValidatorService implements ValidatorService {
  getRowValidator(): FormGroup {
    return new FormGroup({
      'status': new FormControl(null, Validators.required),
      'title': new FormControl()
      });
  }
}