import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Partner } from '../models/partner.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  private serviceUrl = 'http://localhost:5000/api/partners';

  dataChange: BehaviorSubject<Partner[]> = new BehaviorSubject<Partner[]>([]);
  // Temporarily stores data from add and edit dialogs
  dialogData: any;

  constructor(private http: HttpClient) { }

  get data(): Partner[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllPartners(): void {
    this.http.get<Partner[]>(this.serviceUrl).subscribe(data => {
      this.dataChange.next(data);
    },
    (error: HttpErrorResponse) => {
    console.log (error.name + ' ' + error.message);
    });
  }
 

  addPartner (partner: Partner): void {
    this.dialogData = partner;
  }

  updatePartner (partner: Partner): void {
    this.dialogData = partner;
  }

 }
