import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
import { IRootObject } from '../core/interfaces/occasions.inferface';
import { IOccasion } from '../core/interfaces/occasions.inferface';
import { Observable} from 'rxjs';
import { first } from 'rxjs/operators';
import * as cloneDeep from 'lodash/cloneDeep'; 
import * as clone from 'clone';
import 'rxjs/add/operator/map';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class OccasionService {
  occasionsData: Array<IOccasion> = [];
  dataChange: BehaviorSubject<IOccasion[]> = new BehaviorSubject<IOccasion[]>([]);
  // Temporarily stores data from add and edit dialogs
  dialogData: any;

constructor(private httpc:HttpClient) {}
    
  public getrootobject(partner_key: string): Observable<IOccasion[]>
  {
    const headers = new HttpHeaders().set('X-AGI-PARTNER-KEY', partner_key)
                                     .set('Accept', 'applications/json');

    return this.httpc.get<IRootObject>('http://localhost:5000/api/partner', { headers: headers })
    .pipe(
      map(
        (data: any[]) => {
       return this.occasionsData = data['item']['occasions'];
      }), catchError(error => {
         throw new Error('Something went wrong!')
      })
    );

  } 

  get data(): IOccasion[] {
    return this.dataChange.value;
  }
 
  getDialogData() {
    return this.dialogData;
  }

  getAllOccasions(occasions: IOccasion[]): void {
      this.dataChange.next(occasions);
  }

  addOccasion (occasion: IOccasion): void {
    this.dialogData = occasion;
  }

  updateOccasion (occasion: IOccasion): void {
    this.dialogData = occasion;
  }

 }
