import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { User } from '../models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) {}
         
    getAll() {
        const hdrs = new HttpHeaders()
                        .set("Access-Control-Allow-Origin", "*");

        return this.http.get<User[]>('http://localhost:5000/api/users', {headers: hdrs} ) ;
    }

    register(user: User) {
        return this.http.post(`http://localhost:5000/users/register`, user);
    }

}