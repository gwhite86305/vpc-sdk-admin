import { Injectable } from '@angular/core';
import { Subject} from 'rxjs/Subject';
import { Observable} from 'rxjs';

@Injectable()
export class PartnerKeyService {
    myMethod$: Observable<any>;
    private myMethodSubject = new Subject<any>();

    constructor() {
        this.myMethod$ = this.myMethodSubject.asObservable();
    }

    myMethod(data) {
        console.log('mymethod in PartnerKeyService data is:' + data); 
        // we can do stuff with data if we want
        this.myMethodSubject.next(data);
    }
}