export const STATION_TOGGLE_OPTIONS = [
  {
    id: 0,
    label: 'SYSTEM_SETUP.STATION_AREA_TOGGLE',
    dataqaid: 'system-setup-area-toggle',
  },
  {
    id: 1,
    label: 'SYSTEM_SETUP.STATION_CONTROLLER_TOGGLE',
    dataqaid: 'system-setup-controller-toggle',
  },
];

export const ICON_COLOUR = '#008658';
