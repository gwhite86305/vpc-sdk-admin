export interface IColumn {
  headerName: string;
  field?: string;
  type?: Array<string>;
  width?: number;
  minWidth?: number;
  maxWidth?: number;
}
