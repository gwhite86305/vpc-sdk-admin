export interface IRootObject {
  item: IItem;
}
export interface IItem {
  occasions: IOccasion[];
  options: OptionsItem[];
  title: string;
}
export interface IOccasion {
  end_date: Date;
  id: number;
  thumburl: string;
  products: ProductsItem[];
  sort_order: number;
  start_date: Date;
  title: string;
}
export interface ProductsItem {
  assets: Assets;
  content_id: number;
  cover_verse: string;
  inside_verse: string;
  thumb: string;
  title: string;
}
export interface Assets {
  detail_view: string[];
  inside: string[];
  manifest: string[];
  outside: string[];
}
export interface OptionsItem {
  name: string;
  value: string;
}