export interface IToggleOption {
  id: number;
  label: string;
  dataqaid?: string;
}
