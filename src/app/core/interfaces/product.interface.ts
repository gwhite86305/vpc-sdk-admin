export interface IProduct {
    id: number,
    cover_verse: string,
    inside_verse: string,
    title: string,
    content_id: number,
  }