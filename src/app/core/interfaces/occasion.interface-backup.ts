export interface IOccasion {
    item: Item;
}
export interface Item {
    occasions: OccasionsItem[];
    options: OptionsItem[];
    title: string;
}
export interface OccasionsItem {
    id: number;
    products: ProductsItem[];
    title: string;
}
export interface ProductsItem {
    assets: Assets;
    content_id: number;
    cover_verse: string;
    inside_verse: string;
    thumb: string;
    title: string;
}
export interface Assets {
    detail_view: string[];
    inside: string[];
    manifest: string[];
    outside: string[];
}
export interface OptionsItem {
    name: string;
    value: string;
}
