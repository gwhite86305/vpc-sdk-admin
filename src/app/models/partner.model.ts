export interface Partner {
    id: number;
    partner_key: string;
    status: string;
    title: string;
}
