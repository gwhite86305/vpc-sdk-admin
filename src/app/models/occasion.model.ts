export interface Occasion {
    occasions: Array<IOccasion>
}
export interface IOccasion {
    id: number;
    title: string;
    partner_id: number;
    start_date: Date;
    end_date: Date;
    sort_order: Date;
}
