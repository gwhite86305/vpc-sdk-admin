import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login';
import { HomeComponent } from './home';
import { ProductComponent } from '../app/product/product.component';
import { RegisterComponent } from './register';
import { PartnerTableComponent } from '../app/partner-table/partner-table.component';
import { OccasionComponent } from '../app/occasion/occasion.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'partners', component: PartnerTableComponent },
    { path: 'occasions/:partner_key', component: OccasionComponent},
    { path: 'products/:data_id', component: ProductComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);